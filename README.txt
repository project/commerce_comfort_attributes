Introduction:
---

Changes the architecture of working with commercial attributes.
Allows all or some attribute types to be used in 1 variation type field.

Allows you to minimize the creation of many types of variations.


Installation:
---

composer require drupal/commerce_comfort_attributes


Usage:
---

1. Go to admin/commerce/config/product-variation-types/YOUR_VARIATION_TYPE/edit, check "Use comfort attributes." and select available commerce attributes then click "Save".

2. Go to admin/commerce/config/product-variation-types/YOUR_VARIATION_TYPE/edit/form-display, set the "Inline entity form - Complex" widget for the "Attributes" field to use the best UX.

3. Create a product with this type of variation by adding any available attributes.
