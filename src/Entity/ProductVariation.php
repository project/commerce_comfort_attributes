<?php

namespace Drupal\commerce_comfort_attributes\Entity;

use Drupal\commerce_product\Entity\ProductVariation as ProductVariationNative;

class ProductVariation extends ProductVariationNative {

  /**
   * {@inheritdoc}
   */
  public function getAttributeValues() {
    if (!$this->isComfortAttributes()) {
      return parent::getAttributeValues();
    }

    $attribute_values = [];
    foreach ($this->comfort_attributes->referencedEntities() as $attribute) {
      $attribute_values['attribute_' . $attribute->bundle()] = $attribute;
    }
    return $this->ensureTranslations($attribute_values);
  }

  /*
   * Returns 1 if comfort attributes are used 0 otherwise.
   */
  public function isComfortAttributes() {
    // The product variation type is a bundle setting.
    $type_storage = $this->entityTypeManager()->getStorage('commerce_product_variation_type');
    $type_entity = $type_storage->load($this->bundle());

    return $type_entity->getThirdPartySetting('commerce_comfort_attributes', 'comfort_attributes');
  }

  /*
   * Returns attribute value id that is in the $values_ids.
   */
  public function getAttributeComfortValueId(array $value_ids) {
    $target_ids = array_column($this->comfort_attributes->getValue(), 'target_id');
    $attribute_id = current(array_intersect($target_ids, $value_ids));
    return $attribute_id;
  }

}
