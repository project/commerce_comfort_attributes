<?php

namespace Drupal\commerce_comfort_attributes;

use Drupal\commerce_product\ProductAttributeFieldManager as ProductAttributeFieldManagerNative;
use Drupal\commerce_product\Entity\ProductAttributeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

/**
 * Default implementation of the ProductAttributeFieldManagerInterface.
 */
class ProductAttributeFieldManager extends ProductAttributeFieldManagerNative {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function buildFieldMap() {
    $field_map = [];
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo('commerce_product_variation');
    foreach (array_keys($bundle_info) as $bundle) {
      /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
      $form_display = commerce_get_entity_display('commerce_product_variation', $bundle, 'form');
      foreach ($this->getFieldDefinitions($bundle) as $field_name => $definition) {
        $handler_settings = $definition->getSetting('handler_settings');
        $comfort_attributes = $this->isComfortAttributes($bundle);
        if ($comfort_attributes) {
          foreach ($handler_settings['target_bundles'] as $value) {
            $field_map[$bundle][] = [
              'attribute_id' => $value,
              'field_name' => 'attribute_' . $value,
            ];
          }
        } else {
          $component = $form_display->getComponent($field_name);
          $field_map[$bundle][] = [
            'attribute_id' => reset($handler_settings['target_bundles']),
            'field_name' => $field_name,
            'weight' => $component ? $component['weight'] : 0,
          ];
        }
      }

      if (!empty($field_map[$bundle])) {
        uasort($field_map[$bundle], ['\Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
        // Remove the weight keys to reduce the size of the cached field map.
        $field_map[$bundle] = array_map(function ($map) {
          return array_diff_key($map, ['weight' => '']);
        }, $field_map[$bundle]);
      }
    }

    return $field_map;
  }

  /**
   * {@inheritdoc}
   */
  public function createField(ProductAttributeInterface $attribute, $variation_type_id) {
    $comfort_attributes = $this->isComfortAttributes($variation_type_id);
    if (!$comfort_attributes) {
      parent::createField($attribute, $variation_type_id);
    } else {
      $field_name = 'comfort_attributes';
      $field_storage = FieldStorageConfig::loadByName('commerce_product_variation', $field_name);
      $field = FieldConfig::loadByName('commerce_product_variation', $variation_type_id, $field_name);
      if (empty($field_storage)) {
        $field_storage = FieldStorageConfig::create([
          'field_name' => $field_name,
          'entity_type' => 'commerce_product_variation',
          'type' => 'entity_reference',
          'cardinality' => -1,
          'settings' => [
            'target_type' => 'commerce_product_attribute_value',
          ],
          'translatable' => FALSE,
        ]);
        $field_storage->save();
      }
      if (empty($field)) {
        $field = FieldConfig::create([
          'field_storage' => $field_storage,
          'bundle' => $variation_type_id,
          'label' => $this->t('Attributes'),
          'required' => TRUE,
          'settings' => [
            'handler' => 'default',
            'handler_settings' => [
              'target_bundles' => [
                 $attribute->id() => $attribute->id()
               ],
            ],
          ],
          'translatable' => FALSE,
        ]);
        $field->save();

        /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
        $form_display = commerce_get_entity_display('commerce_product_variation', $variation_type_id, 'form');
        $form_display->setComponent($field_name, [
          'type' => 'options_select',
          'weight' => $this->getHighestWeight($form_display) + 1,
        ]);
        $form_display->save();
      } else {
        // Add available attribute.
        $handler_settings = $field->getSetting('handler_settings');
        $handler_settings['target_bundles'][$attribute->id()] = $attribute->id();
        $field->setSetting('handler_settings', $handler_settings);
        $field->save();
      }
      $this->clearCaches();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function canDeleteField(ProductAttributeInterface $attribute, $variation_type_id) {
    $comfort_attributes = $this->isComfortAttributes($variation_type_id);
    if (!$comfort_attributes) {
      return parent::canDeleteField($attribute, $variation_type_id);
    }

    // @TODO canDeleteField() for this case.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteField(ProductAttributeInterface $attribute, $variation_type_id) {
    $comfort_attributes = $this->isComfortAttributes($variation_type_id);
    if (!$comfort_attributes) {
      parent::deleteField($attribute, $variation_type_id);
    } else {
      $field_name = 'comfort_attributes';
      $field = FieldConfig::loadByName('commerce_product_variation', $variation_type_id, $field_name);
      if ($field) {
        $handler_settings = $field->getSetting('handler_settings');
        if (count($handler_settings['target_bundles']) > 1) {

          // @TODO canDeleteField() for this case.
          unset($handler_settings['target_bundles'][$attribute->id()]);
          $field->setSetting('handler_settings', $handler_settings);
          $field->save();
        } else {
          // Now we do not refer to any type of attribute,
          // then we can delete the entire field.

          // canDeleteField() for this case.
          $query = $this->entityTypeManager->getStorage('commerce_product_variation')->getQuery()
            ->condition('type', $variation_type_id)
            ->exists($field_name)
            ->range(0, 1);

          if (empty($query->execute())) {
            $field->delete();
          }
        }
        $this->clearCaches();
      }
    }
  }

  /*
   * Returns 1 if comfort attributes are used 0 otherwise.
   */
  public function isComfortAttributes($variation_type_id) {
    return $this->entityTypeManager
      ->getStorage('commerce_product_variation_type')
      ->load($variation_type_id)
      ->getThirdPartySetting('commerce_comfort_attributes', 'comfort_attributes');
  }

}
