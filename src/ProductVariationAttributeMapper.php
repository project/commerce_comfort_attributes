<?php

namespace Drupal\commerce_comfort_attributes;

use Drupal\commerce_product\ProductVariationAttributeMapper as ProductVariationAttributeMapperNative;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product\PreparedAttribute;

class ProductVariationAttributeMapper extends ProductVariationAttributeMapperNative {

  /**
   * {@inheritdoc}
   */
  public function selectVariation(array $variations, array $attribute_values = [], $triggering_element = NULL) {
    $is_comfort_attributes = current($variations)->isComfortAttributes() ? TRUE : FALSE;
    if (!$is_comfort_attributes) {
      return parent::selectVariation($variations, $attribute_values);
    }

    $selected_variation = NULL;
    $return_value = $triggering_element['#value'];
    $return_value = is_numeric($return_value) ? $return_value : FALSE;
    $default_value = $triggering_element['#default_value'] ?? FALSE;

    foreach ($variations as $variation) {
      $attributes_ids = array_column($variation->comfort_attributes->getValue(), 'target_id');
      // What was not needed.
      if ($default_value && in_array($default_value, $attributes_ids)) {
        continue;
      }
      // What is needed now.
      if ($return_value && !in_array($return_value, $attributes_ids)) {
        continue;
      }
      // Finding the largest number of attribute matches.
      $diff = array_diff($attribute_values, $attributes_ids);
      if (!$diff) {
        return $variation;
      }
      if (!isset($count_diff)) {
        $count_diff = count($diff);
        $selected_variation = $variation;
      } elseif (count($diff) < $count_diff) {
        $selected_variation = $variation;
      }
    }

    return $selected_variation;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareAttributes(ProductVariationInterface $selected_variation, array $variations) {
    if (!$selected_variation->isComfortAttributes()) {
      return parent::prepareAttributes($selected_variation, $variations);
    }

    $attributes = [];

    // Prepare all attribute ids of current variations.
    $variations_attribute_ids = [];
    foreach ($variations as $variation) {
      $target_ids = array_column($variation->comfort_attributes->getValue(), 'target_id');
      $variations_attribute_ids = array_unique(array_merge($variations_attribute_ids, $target_ids));
    }

    // Prepare definitions.
    foreach ($variations_attribute_ids as $attribute_id) {
      $attribute_value = $this->entityRepository->getCanonical('commerce_product_attribute_value', $attribute_id);
      $attribute_id = $attribute_value->bundle();
      $field_name = 'attribute_' . $attribute_id;
      if (!isset($definitions[$field_name])) {
        /** @var \Drupal\commerce_product\Entity\ProductAttributeInterface $attribute */
        $attribute = $this->attributeStorage->load($attribute_id);
        // Make sure we have translation for attribute.
        $attribute = $this->entityRepository->getTranslationFromContext($attribute, $selected_variation->language()->getId());

        $definitions[$field_name] = [
          'id' => $attribute->id(),
          'label' => $attribute->label(),
          'element_type' => $attribute->getElementType(),
          'required' => TRUE,
        ];
      }
      $definitions[$field_name]['values'][ $attribute_value->id()] = $attribute_value->label();
    }

    if (isset($definitions)) {
      // Prepare attributes.
      foreach ($definitions as $field_name => $definition) {
        $attributes[$field_name] = new PreparedAttribute($definition);
      }
    }

    return $attributes;
  }

}
